#!/usr/bin/env bash

# Install SDKMAN
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"

# Install Java
sdk install java 11.0.11.j9-adpt
sdk install java 16.0.1.j9-adpt
sdk default java 11.0.11.j9-adpt
