#!/usr/bin/env bash

# Run the Homebrew Scripts
/bin/bash -c "$(curl -fsSL https://gitlab.com/johnky.nguyen/dotfiles/-/raw/main/brew.sh)"
/bin/bash -c "$(curl -fsSL https://gitlab.com/johnky.nguyen/dotfiles/-/raw/main/brew-java.sh)"
