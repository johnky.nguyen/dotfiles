#!/usr/bin/env bash

brew install php@7.4

# Install Symfony-cli
curl -sS https://get.symfony.com/cli/installer | bash
echo 'export PATH="$HOME/.symfony/bin:$PATH"' >> ~/.zshrc
