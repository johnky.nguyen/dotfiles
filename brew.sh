#!/usr/bin/env bash

# Install Brew Packages

# IDEs
brew install --cask jetbrains-toolbox
brew install --cask atom
brew install --cask sublime-text

# Required at Tamara
brew install oci-cli
brew install kubernetes-cli
brew install bash-completion
echo "[[ $commands[kubectl] ]] && source <(kubectl completion zsh)" >> ~/.zshrc
brew install helm
brew install redis
brew install --cask tunnelblick
brew install mysql-client
echo 'export PATH="/opt/homebrew/opt/mysql-client/bin:$PATH"' >> ~/.zshrc

# Common CLIs
brew install tree
brew install git

# Development tools
brew install --cask docker
brew install minikube
brew install --cask postman
brew install zsh
brew install --cask iterm2

# Communication apps
brew install --cask slack
