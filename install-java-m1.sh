#!/usr/bin/env bash

# Install SDKMAN
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"

# Install Java
sdk install java 11.0.12-zulu
sdk install java 16.0.2-zulu
sdk default java 11.0.12-zulu
