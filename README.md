# Mac Setup
** Note, no need to clone this repository, just follow the below instruction for setup your local Java development on a new Mac machine.

## Install
#### Install Homebrew
```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

#### Install Java
```
/bin/bash -c "$(curl -fsSL https://gitlab.com/johnky.nguyen/dotfiles/-/raw/main/install-java.sh)"
```
For Mac M1:
```
/bin/bash -c "$(curl -fsSL https://gitlab.com/johnky.nguyen/dotfiles/-/raw/main/install-java-m1.sh)"
```

#### Install PHP
```
/bin/bash -c "$(curl -fsSL https://gitlab.com/johnky.nguyen/dotfiles/-/raw/main/install-php.sh)"
```

#### Install brew packages
```
/bin/bash -c "$(curl -fsSL https://gitlab.com/johnky.nguyen/dotfiles/-/raw/main/brew.sh)"
```

#### Install additional facilities
```
/bin/bash -c "$(curl -fsSL https://gitlab.com/johnky.nguyen/dotfiles/-/raw/main/brew-optional.sh)"
```
