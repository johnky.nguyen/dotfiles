#!/usr/bin/env bash

# Install Brew Packages

# Utilities
brew install --cask rectangle
brew cask install sourcetree
brew install --cask google-drive

# Communication apps
brew install --cask telegram
brew install --cask viber

# Development tools
brew install --cask tableplus
